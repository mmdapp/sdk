const { log: LOG, error: ERR } = console

async function init () {
  LOG('SDK Init')
  ERR('test ERR')
}

async function sub () {
  LOG('SDK subscriptions')
}

export const SDK = {
  init,
  sub
}
